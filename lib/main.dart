import 'package:flutter/material.dart';
import 'package:start_flutter/screen/home/home_page.dart';

void main() {
  runApp(MyApp());
}

// App này chạy trên phiên bản Flutter SDK 1.22.5
class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'List country',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
