import 'package:dio/dio.dart';

enum RequestMethod { POST, GET }

class BaseRequest {
  static Dio dio = Dio();
  static final _singleton = BaseRequest._internal();

  factory BaseRequest() {
    dio.options.connectTimeout = 15000;
    dio.options.baseUrl = 'https://countryapi.gear.host';
    return _singleton;
  }
  BaseRequest._internal();

  Future<dynamic> sendRequest(String path, RequestMethod requestMethod,
      {dynamic jsonMap, dynamic queryParams}) async {
    Response response;

    if (requestMethod == RequestMethod.POST) {
      response =
          await dio.post(path, data: jsonMap, queryParameters: queryParams);
    } else if (requestMethod == RequestMethod.GET) {
      response = await dio.get(
        path,
        queryParameters: queryParams,
      );
    }
    return response.data;
  }
}
