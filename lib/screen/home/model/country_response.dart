class CountryResponse {
  CountryResponse({
    this.isSuccess,
    this.userMessage,
    this.technicalMessage,
    this.totalCount,
    this.response,
  });

  bool isSuccess;
  dynamic userMessage;
  dynamic technicalMessage;
  int totalCount;
  List<Response> response;

  factory CountryResponse.fromJson(Map<String, dynamic> json) =>
      CountryResponse(
        isSuccess: json["IsSuccess"],
        userMessage: json["UserMessage"],
        technicalMessage: json["TechnicalMessage"],
        totalCount: json["TotalCount"],
        response: List<Response>.from(
            json["Response"].map((x) => Response.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "IsSuccess": isSuccess,
        "UserMessage": userMessage,
        "TechnicalMessage": technicalMessage,
        "TotalCount": totalCount,
        "Response": List<dynamic>.from(response.map((x) => x.toJson())),
      };
}

class Response {
  Response({
    this.name,
    this.alpha2Code,
    this.alpha3Code,
    this.nativeName,
    this.region,
    this.subRegion,
    this.latitude,
    this.longitude,
    this.area,
    this.numericCode,
    this.nativeLanguage,
    this.currencyCode,
    this.currencyName,
    this.currencySymbol,
    this.flag,
    this.flagPng,
  });

  String name;
  String alpha2Code;
  String alpha3Code;
  String nativeName;
  Region region;
  String subRegion;
  String latitude;
  String longitude;
  int area;
  int numericCode;
  String nativeLanguage;
  String currencyCode;
  String currencyName;
  String currencySymbol;
  String flag;
  String flagPng;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        name: json["Name"],
        alpha2Code: json["Alpha2Code"],
        alpha3Code: json["Alpha3Code"],
        nativeName: json["NativeName"],
        region: regionValues.map[json["Region"]],
        subRegion: json["SubRegion"],
        latitude: json["Latitude"],
        longitude: json["Longitude"],
        area: json["Area"] == null ? null : json["Area"],
        numericCode: json["NumericCode"] == null ? null : json["NumericCode"],
        nativeLanguage: json["NativeLanguage"],
        currencyCode: json["CurrencyCode"],
        currencyName: json["CurrencyName"],
        currencySymbol: json["CurrencySymbol"],
        flag: json["Flag"],
        flagPng: json["FlagPng"],
      );

  Map<String, dynamic> toJson() => {
        "Name": name,
        "Alpha2Code": alpha2Code,
        "Alpha3Code": alpha3Code,
        "NativeName": nativeName,
        "Region": regionValues.reverse[region],
        "SubRegion": subRegion,
        "Latitude": latitude,
        "Longitude": longitude,
        "Area": area == null ? null : area,
        "NumericCode": numericCode == null ? null : numericCode,
        "NativeLanguage": nativeLanguage,
        "CurrencyCode": currencyCode,
        "CurrencyName": currencyName,
        "CurrencySymbol": currencySymbol,
        "Flag": flag,
        "FlagPng": flagPng,
      };
}

enum Region { ASIA, EUROPE, AFRICA, OCEANIA, AMERICAS, POLAR, EMPTY }

final regionValues = EnumValues({
  "Africa": Region.AFRICA,
  "Americas": Region.AMERICAS,
  "Asia": Region.ASIA,
  "": Region.EMPTY,
  "Europe": Region.EUROPE,
  "Oceania": Region.OCEANIA,
  "Polar": Region.POLAR
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
