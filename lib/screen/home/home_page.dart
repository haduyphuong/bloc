import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:start_flutter/screen/home/bloc/home_bloc.dart';
import 'package:start_flutter/screen/home/model/country_response.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  CountryResponse country;
  final _bloc = HomeBloc();

  @override
  void initState() {
    _bloc.add(GetListCountry());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("List country"),
      ),
      body: BlocProvider(
        create: (context) => _bloc,
        child: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            if (state is GetCountrySuccess) {
              country = state.response;
            } else if (state is GetCountryFailed) {
              _showError(state.error);
            }
          },
          builder: (context, state) {
            return Center(child: _buildListCountry());
          },
        ),
      ),
    );
  }

  Widget _buildListCountry() {
    return country != null
        ? ListView.separated(
            itemCount: country.response.length,
            separatorBuilder: (context, index) => Divider(),
            itemBuilder: (context, index) {
              return Card(
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.network(
                          country.response[index].flagPng,
                          height: 60,
                          fit: BoxFit.fill,
                        )),
                    Expanded(
                        flex: 3,
                        child: Column(
                          children: [
                            ListTile(
                              trailing:
                                  Text(country.response[index].alpha2Code),
                              title: Text(country.response[index].name),
                            )
                          ],
                        ))
                  ],
                ),
              );
            },
          )
        : CupertinoActivityIndicator();
  }

  void _showError(String error) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(error),
      duration: Duration(seconds: 10),
      action: SnackBarAction(
          label: 'Thử lại',
          onPressed: () => _bloc.add(
                GetListCountry(),
              )),
    ));
  }
}
