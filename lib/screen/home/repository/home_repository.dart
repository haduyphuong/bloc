import 'package:start_flutter/screen/home/model/country_response.dart';
import 'package:start_flutter/services/base_request.dart';

class HomeRepository {
  final _baseRequest = BaseRequest();

  Future<CountryResponse> getListCoutry() async {
    final result = await _baseRequest.sendRequest(
        '/v1/Country/getCountries', RequestMethod.GET);
    return CountryResponse.fromJson(result);
  }
}
