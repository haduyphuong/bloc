import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:start_flutter/screen/home/model/country_response.dart';
import 'package:start_flutter/screen/home/repository/home_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial());

  final _homeRepository = HomeRepository();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    yield HomeInitial(); // Bắt buộc gọi mỗi lần, nên đặt ở đầu.
    if (event is GetListCountry) {
      yield* _getListCountry();
    }
  }

  Stream<HomeState> _getListCountry() async* {
    // Lây data từ repository.
    try {
      final result = await _homeRepository.getListCoutry();
      // Trả dữ liệu về qua state để ở view nhận được.
      yield GetCountrySuccess(result);
    } on DioError catch (e) {
      yield GetCountryFailed(e.message.toString());
    }
  }
}
