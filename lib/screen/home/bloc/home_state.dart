part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class GetCountrySuccess extends HomeState {
  final CountryResponse response;

  GetCountrySuccess(this.response);
}

class GetCountryFailed extends HomeState {
  final String error;

  GetCountryFailed(this.error);
}
